from django.contrib import admin
from projects.models import Project,Technology

class TechnologyAdmin(admin.ModelAdmin):
    list_display = ('techName',)

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('projectName',)


admin.site.register(Project,ProjectAdmin)
admin.site.register(Technology,TechnologyAdmin)
