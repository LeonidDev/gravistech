import os

from django.db import models
from django.db.models.signals import post_save,pre_delete


class Project(models.Model):
    """ docstring for Product """
    projectName = models.CharField(max_length=50,blank=False,unique=True,)
    projectImage = models.ImageField(upload_to='gravistech/static/img/',blank=True)
    projectUrl = models.URLField(max_length=50,blank=True,)
    projectDone = models.DateField(auto_now=False,)
    projectDescription = models.TextField(max_length=300)
    isDone = models.BooleanField(default=False,)

    def __init__(self, *args, **kwargs):
        super(Project, self).__init__(*args, **kwargs)
        self.args = args

    def __unicode__(self):
        return self.projectName

    def filename(self):
        return os.path.basename(self.techImage.name)

    class Meta():
        ordering = ('projectName',)
        verbose_name = "Project"
        verbose_name_plural = "Project"


class Technology(models.Model):
    """ docstring for Technology """
    techName = models.CharField(max_length=50,blank=False,unique=True,)
    projects = models.ManyToManyField(Project)
    techImage = models.ImageField(upload_to="gravistech/static/img/")

    def __init__(self, *args, **kwargs):
        super(Technology, self).__init__(*args, **kwargs)
        self.args = args

    def __unicode__(self):
        return self.techName

    def filename(self):
        return os.path.basename(self.techImage.name)

    class Meta():
        ordering = ('techName',)
        verbose_name = "Technology"
        verbose_name_plural = "Technologies"

def delete_image(sender, instance, **kwargs):
    if sender == Technology:
        tech = instance
        file_path = os.path.dirname(os.path.dirname(__file__)) + '/gravistech/static/' + tech.techImage.name
        os.remove(file_path)
    elif sender == Project:
        project = instance
        file_path = os.path.dirname(os.path.dirname(__file__)) + '/gravistech/static/' + project.projectImage.name
        os.remove(file_path)
    else:
        pass

def delete_string(sender, instance, **kwargs):
###This function reconfigures the image paths for referencing images
    if sender == Technology:
        tech = instance
        if "gravistech/static/" in tech.techImage.name:
            tech.techImage.name = tech.techImage.name.split("gravistech/static/")[1]
            tech.save()
    elif sender == Project:
        project = instance
        if "gravistech/static/" in project.projectImage.name:
            project.projectImage.name = project.projectImage.name.split("gravistech/static/")[1]
            project.save()

post_save.connect(delete_string,sender=Technology)
pre_delete.connect(delete_image,sender=Technology)
post_save.connect(delete_string,sender=Project)
pre_delete.connect(delete_image,sender=Project)