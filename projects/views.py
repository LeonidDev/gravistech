from django.shortcuts import render_to_response
from django.templates import RequestContext

from projects.models import Project, Technology
from employees.models import Individual

def home(request):
	projects = Project.objects.all()
	individuals = Individual.objects.all()
	return render_to_response('index.html',{'projects':projects,'individuals':individuals},context_instance=RequestContext(request))