from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'gravistech.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'landing.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^contact/', include('contact_form.urls')),
    #url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': path.join(path.dirname(__file__), 'static')}),

)

urlpatterns += staticfiles_urlpatterns()
