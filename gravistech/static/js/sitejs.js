jQuery(document).ready(function ($) {
 
 
    //initialise Stellar.js
    $(window).stellar();
 
    //Cache some variables
    var links = $('.navigation').find('li');
    slide = $('.slide');
    mywindow = $(window);
    htmlbody = $('html,body');
 
 
    //Setup waypoints plugin
    slide.waypoint(function (event, direction) {
 
        //cache the variable of the data-slide attribute associated with each slide
        dataslide = $(this).attr('data-slide');
 
        //If the user scrolls up change the navigation link that has the same data-slide attribute as the slide to active and 
        //remove the active class from the previous navigation link 
        if (direction === 'down') {
            $('.navigation li[data-slide="' + dataslide + '"]').addClass('active').prev().removeClass('active');
        }
        // else If the user scrolls down change the navigation link that has the same data-slide attribute as the slide to active and 
        //remove the active class from the next navigation link 
        else {
            $('.navigation li[data-slide="' + dataslide + '"]').addClass('active').next().removeClass('active');

        }
 
    });
 
    //waypoints doesnt detect the first slide when user scrolls back up to the top so we add this little bit of code, that removes the class 
    //from navigation link slide 2 and adds it to navigation link slide 1. 
    mywindow.scroll(function () {
        if (mywindow.scrollTop() == 0) {
            $('.navigation li[data-slide="1"]').addClass('active');
            $('.navigation li[data-slide="2"]').removeClass('active');
        }
    });
 
    //Create a function that will be passed a slide number and then will scroll to that slide using jquerys animate. The Jquery
    //easing plugin is also used, so we passed in the easing method of 'easeInOutQuint' which is available throught the plugin.
    function goToByScroll(dataslide) {
        htmlbody.animate({
            scrollTop: $('.slide[data-slide="' + dataslide + '"]').offset().top
        }, 2000, 'easeInOutQuint');
    }
 
 
 
    //When the user clicks on the navigation links, get the data-slide attribute value of the link and pass that variable to the goToByScroll function
    links.click(function (e) {
        e.preventDefault();
        dataslide = $(this).attr('data-slide');
        goToByScroll(dataslide);
    });
 
    
    var terms = ["Efficiency", "Transparency", "Predictability", "Affordability"];
    //this function is responsible for the word rotations.. it fades in different words in the array..
    function rotateTerm() {
      var ct = $("#rotate").data("term") || 0;
      $("#rotate").data("term", ct == terms.length -1 ? 0 : ct + 1).text(terms[ct]).fadeIn(2000)
                  .delay(4000).fadeOut(2000, rotateTerm);
    }
    $(rotateTerm);

    
    //------- masonry js for the technologies and projects in landing page
    $('#techs').masonry({
        // options here
        columnWidth: 200,
        itemSelector: '.item',
        gutter:10,
        isFitWidth:true,
    });
    $('#projects-container').masonry({
        itemSelector: '.project-masonry-item',
        gutter:30,
        isFitWidth:true,
    });
    //----------------------------------------
    //-----Google Maps javascript
    function initialize() {
        var mapOptions = {
            zoom: 18,
            center: new google.maps.LatLng(7.0596224, 125.5293507),
            scrollwheel: false,
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'),
                                    mapOptions);
        setMarkers(map,address_point);
    }
    var address_point = ['Gravis Headquarters',7.0596224,125.5307092];
    function setMarkers(map, locations){
        var image = {
            url: 'static/img/maplogo.png',
            size: new google.maps.Size(20, 21),
            origin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(10, 10),
        };
        var address = address_point;
        var myLatLng = new google.maps.LatLng(7.0596224,125.5307092);
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
           icon: image,
            title: 'Gravis Technologies Headquarters',
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    /*================
        Contact Form
    =================*/
    function show_error(){
        if($("#form_error").length == 0){
            var error_msg = "&nbsp;<small style='color:red;'id='form_error'>Must be a valid email</small";
            $("#id_email").after(error_msg);
            $("#form_error").hide().fadeIn(2000);
        }
        else{
            $("#form_error").hide().fadeIn(2000);
        }
    }
    $("#id_email").blur(function(e){
        if(!validateForm()){
            show_error();
        }else{
            if(!($("#form_error").length == 0)){
                $("#form_error").fadeOut(1000, function(e){
                    $(this).remove();
                })
            }
        }
    });
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    };
    function validateForm(){
        if(isValidEmailAddress($("#id_email").val())){
            return true;
        }
        else{
            return false;
        }
    }
    $("#submit-button").click(function(e){
        if(validateForm()){
            $("#submit").submit();
        }
        else{
            show_error();
        }
    });
});