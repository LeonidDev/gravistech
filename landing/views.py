import os.path

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect

from contact_form.forms import ContactForm

from projects.models import Project, Technology
from employees.models import Individual

def home(request):
    projects = Project.objects.all()
    technology = Technology.objects.all()
    if request.method == "POST":
        form = ContactForm(request.POST,request=request)
        if form.is_valid():
            print form
            form.save()
            print("success")
            return HttpResponseRedirect('/')
    else:
        form = ContactForm(request=request)
        return render_to_response('landing/landing.html',
            {'projects':projects,'technologies':technology,
            'contact_form':form},
            context_instance=RequestContext(request))
