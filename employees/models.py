from django.db import models
from django.contrib.auth.models import User


class Individual(models.Model):
    educationalChoices = (
        ('hs', 'Less than highschool'),
        ('coll', 'College'),
        ('ms', 'Proficient'),
        ('exp', 'Expert'),
    )
    user = models.OneToOneField(User)
    jobTitle = models.ForeignKey('employees.EmployeeJobJoint')
    jobType = models.ForeignKey('employees.EmploymentRelations')
    employmentDate = models.DateField(auto_now=False,)
    profilePicture = models.ImageField(upload_to="/")
    educationalAttainment = models.CharField(max_length=100,choices=educationalChoices,help_text="Enter employee's highest educational attainment.")
    motto = models.CharField(max_length=100,)


class EmployeeTechnologyJoint(models.Model):
    levels = (
        ('bg', 'Beginner'),
        ('imd', 'Intermediate'),
        ('pro', 'Proficient'),
        ('exp', 'Expert'),
    )
    employee = models.ForeignKey(Individual)
    relation = models.ForeignKey('projects.Technology')
    expertiseLevel = models.CharField(max_length=60, blank=False, choices=levels)


class JobTitles(models.Model):
    jobTitle = models.CharField(max_length=60, blank=False, unique=True)
    jobDescription = models.TextField(max_length=500, blank=True,)
    abbv = models.CharField(max_length=60, blank=False, unique=True)
    salary = models.FloatField(max_length=90, blank=False,)


class EmploymentRelations(models.Model):
    relationName = models.CharField(max_length=60, blank=False, unique=True)
    description = models.TextField(max_length=500, blank=True,)

class EmployeeJobJoint(models.Model):
    employee = models.ForeignKey(Individual)
    jobTitles = models.ForeignKey(JobTitles)
    jobStarted = models.DateField(auto_now=False,)
    jobEnded = models.DateField(blank=True,)
    employmentStatus = models.BooleanField(default=False,)